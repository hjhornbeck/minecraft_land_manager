# some helper functions and data related to claims

##### IMPORTS

import pandas as pd
import re
from typing import Optional, Union


##### GLOBALS

policies = {	# first = default
				'shared' :'Heavy restrictions on use, ask before entering.',
				'public' :'Claimed but with few restrictions. Unexpected visitors are A-OK.',
				'private':'Do not enter unless invited.' 
	}

claim_parse = re.compile( \
	r'\((-?[\d\.]+)[,/](-?[\d\.]+)\) ?->? ?\((-?[\d\.]+)[,/](-?[\d\.]+)\)[ \t]*(\[[^\]]*\])?[ \t]*([^\n\[\]]+)' \
	)
mod_whitespace = re.compile( r'[,;\t"\']+' )


##### FUNCTIONS

def parse_claim( string: str, world_map: dict[str,str] ) -> \
		Union[str, tuple[str, int, int, int, int, str, str]]:
	"""
	Parse the claim portion of the command.

	string: The incoming string to parse.
		world_map: A dictionary where the keys are valid world names

	Returns (world [str], min_x, max_x, min_z, max_z[ints], policy, description[strs]) on success,
		a string detailing the error on failure.
	"""

	# check if we have something legit
	output = claim_parse.match( string )
	if output is None:
		return "I could not understand that claim. Did you follow the proper format?"

	try:
		numeric = [ int(float(x)) for x in output.groups()[:4] ]
	except ValueError as e:
		# TODO: log the error
		return "One of the coordinates was not an integer."

	min_x = min( numeric[0], numeric[2] )
	max_x = max( numeric[0], numeric[2] )
	min_z = min( numeric[1], numeric[3] )
	max_z = max( numeric[1], numeric[3] )

	world = None								# need these shortly
	policy = None

	if output.groups()[4] not in {None,'[]'}:   # only scan if there's something to scan

		subset = output.groups()[4][1:-1]	   # strip out [] brackets and tokenize
		tokens = [x.strip() for x in mod_whitespace.split( subset ) if x != '']

		if len(tokens) > 2:					 # too many tokens is an error
			return "Too many modifiers; you're only allowed one world and one policy."

		for token in tokens:

			cands = [(key,True) for key in world_map.keys() if key.startswith(token)] + \
				[(key,False) for key in policies.keys() if key.startswith(token)]

			if len(cands) > 1:
				return "Ambiguous modifier given, could have been any of " + \
						", ".join([f'"{x}"' for x,_ in cands]) + "."

			if len(cands) == 0:
				return f'Unknown modifier given ("{token}").'

			if cands[0][1]:
				world = cands[0][0]
			else:
				policy = cands[0][0]
				
	if world is None:
		world  = list(world_map.keys())[0]	 # exploit the fact that dictionaries preserve the order of insertion
	if policy is None:
		policy = list(policies.keys())[0]

	return (world, min_x, max_x, min_z, max_z, policy, output.groups()[5])

def find_collisions( database: pd.DataFrame, bound: tuple[str,int,int,int,int] ) -> \
		Optional[pd.DataFrame]:
		"""
		Scan the database of claims for any sign of a collision.

		database: A Pandas DataFrame to scan.
		bound: A quintuple of (world, min X, max X, min Z, max Z). "world" is a string,
				the rest are integers.

		Returns a Dataframe consisting of all collisions, or None if no collisions were found.
		"""
		
		# shave down the world first
		mask = database['world'] == bound[0]
		temp = database[mask]
		if len(temp) == 0:
			return

		# basic algorithm: iterative overlapping lines. Should be plenty fast for small N.
		mask = temp['max x'] >= bound[1]
		temp = temp[mask]
		if len(temp) == 0:
			return

		mask = temp['min x'] <= bound[2]
		temp = temp[mask]
		if len(temp) == 0:
			return

		mask = temp['max z'] >= bound[3]
		temp = temp[mask]
		if len(temp) == 0:
			return

		mask = temp['min z'] <= bound[4]
		temp = temp[mask]
		if len(temp) == 0:
			return

		# if there's anything left, it must be an overlap
		return temp

def release_claim( database: pd.DataFrame, player: str, bound: tuple[str,int,int,int,int] ) -> \
		Optional[pd.DataFrame]:
		"""
		Release a land claim by a given player. A perfect match is required!

		database: A Pandas DataFrame to scan.
		player: The player who is relinquishing the claim.
		bound: A quintuple of (world, min X, max X, min Z, max Z). "world" is a string,
				the rest are integers.

		Returns either a Dataframe missing the claim if successful, or None on failure.
		"""
		
		# best approach: shave the dataset down, coords first
		mask = database['min x'] == bound[1]
		temp = database[mask]
		if len(temp) == 0:
			return

		mask = temp['max x'] == bound[2]
		temp = temp[mask]
		if len(temp) == 0:
			return

		mask = temp['min z'] == bound[3]
		temp = temp[mask]
		if len(temp) == 0:
			return

		mask = temp['max z'] == bound[4]
		temp = temp[mask]
		if len(temp) == 0:
			return

		# by now the search should be quite quick
		mask = temp['world'] == bound[0]
		temp = temp[mask]
		if len(temp) == 0:
			return

		mask = temp['player'] == player
		temp = temp[mask]
		if len(temp) == 0:
			return

#		assert len(temp) == 1	  # if there are dupes, they should all be removed!
		return database.drop( [idx for idx,_ in temp.iterrows()] )  # handy tip: the indicies of the subset line up with the superset

