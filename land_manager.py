#!/usr/bin/env python3

##### IMPORTS

import argparse
import discord
from datetime import datetime
from dotenv import load_dotenv
from os import getenv
import pandas as pd
from pathlib import Path
from pyhocon import ConfigFactory
from subprocess import run
from sys import exit

from claims import find_collisions, parse_claim, policies, release_claim
from Bluemap import read_bluemap, update_bluemap


##### GLOBALS

load_dotenv()
TOKEN = getenv('TOKEN')

database = pd.DataFrame( { 'datetime':[], 'player':[], 'world':[], 'min x':[], \
				'max x':[], 'min z':[], 'max z':[], 'policy':[], \
				'description':[], 'jump_url':[] } )

world_map 	= dict() # records how worlds map to config files
base_configs 	= list() 


##### FUNCTIONS

@client.event
async def on_ready():
    print(f'Land manager has logged on, as {client.user}.')

@client.event
async def on_message(message: discord.Message):
    # ignore our own messages
    if message.author == client.user:
        return

    # check if we were called on to perform a task
    if message.content.startswith('/land-available '):

	    # parse the claim (after 'available', up to first newline)
        first_nl = message.content.find("\n")
        if first_nl < 0:
            first_nl = len(message.content)

        result = parse_claim( message.content[16:first_nl], world_map )
        if type(result) is str:
            await message.channel.send( result )
            return

        world, min_x, max_x, min_z, max_z, policy, desc = result

	    # check if it conflicts
        result = find_collisions( database, (min_x, max_x, min_z, max_z) )
        if type(result) is str:
            await message.channel.send( result )
            return

	    # return the appropriate message
        if result is not None:
            await message.channel.send( 'That rectangle has been claimed! The details are in some of the following messages:\n' + \
                    "\n".join([x['jump_url'] for x in result[:4]]) )      # TODO: Fancier markdown?
        else:
            await message.channel.send('That rectangle of land is available.')

        return

    elif message.content.startswith('/land-claim '):

	# parse the claim (after 'available', up to first newline)
        first_nl = message.content.find("\n")
        if first_nl < 0:
            first_nl = len(message.content)

        result = parse_claim( message.content[12:first_nl], world_map )
        if type(result) is str:
            await message.channel.send( result )
            return

        world, min_x, max_x, min_z, max_z, policy, desc = result

	    # check if it conflicts
        result = find_collisions( database, (min_x, max_x, min_z, max_z) )
        if type(result) is str:
            await message.channel.send( result )
            return

	    # if a conflict, complain and exit
        if result is not None:
            await message.channel.send( 'That rectangle has been claimed! The details are in some of the following messages:\n' + \
                    "\n".join([x['jump_url'] for x in result[:4]]) )      # TODO: fancier markdown?
            return

        # if not, store what we have in the DB
        temp = pd.Dataframe( { 'datetime':[pd.to_datetime(datetime.now())], 'player':[message.author.name], 'world':[world], \
                'min_x':[min_x], 'max_x':[max_x], 'min_z':[min_z], 'max_z':[max_z], 'policy':[policy], \
				'description':[desc], 'jump_url':[message.jump_url] } )
        database = pd.concat( database, temp )
	if args.dump:
		args.dump.seek( 0 )
		database.to_csv( args.dump )
		args.dump.flush()

	#  update Bluemap, and if asked to reload the config
        update_bluemap( base_configs, args.outputdir, database, list(world_map.keys())[0] )
        if args.reload:
            run( args.reload, timeout=args.reload_timeout )

	#  and communicate success
        await message.channel.send('Your claim was successful!')

    elif message.content.startswith('/land-release '):

	# parse the claim (after 'available', up to first newline)
        first_nl = message.content.find("\n")
        if first_nl < 0:
            first_nl = len(message.content)

        result = parse_claim( message.content[12:first_nl], world_map )
        if type(result) is str:
            await message.channel.send( result )
            return

        world, min_x, max_x, min_z, max_z, policy, desc = result

	# check if it conflicts
	result = release_claim( database, message.author.name, (world,min_x,max_x,min_z,max_z) )
	if result is None:
        	await message.channel.send("That claim doesn't exist, or is another user's. Double-check your typing.")
	else:
		#  success! 
		database = result
		if args.dump:
			args.dump.seek( 0 )
			database.to_csv( args.dump )
			args.dump.flush()
        	update_bluemap( base_configs, args.outputdir, database, list(world_map.keys())[0] )
        	if args.reload:
            		run( args.reload, timeout=args.reload_timeout )
        	await message.channel.send("The claim has been released.")
		
    elif message.content.startswith('/land-worlds'):

        await message.channel.send('The following worlds are available: ' + \
		', '.join([f'"{x}"' for x in world_map.keys()]) + '.' )

    elif message.content.startswith('/land-policies'):
        await message.channel.send( 'The following policies are available for claims:\n\n' + \
		'\n'.join([f'"{x}":{policies[x]}' for x in policies.keys()]) )

    elif message.content.startswith('/land-commands'):
        await message.channel.send('''Available commands:

/land-commands
   This message.

/land-worlds
   Print out the available worlds.

/land-policies
   Print out the available land-use policies.

/land-available (X/Z -> X/Z) [MODIFIERS]
   Check if the given rectangle of land is available. MODIFIERS only includes the world to search.

/land-claim (X/Z -> X/Z) [MODIFIERS] NAME
   Claim a rectangle of land. MODIFIERS includes the world and policy for the claim.

/land-release (X/Z -> X/Z) [MODIFIERS]
   Release the given claim. The coordinates must match exactly! MODIFIERS only includes the world to search.''')


##### MAIN

# handle command line arguments
parser = argparse.ArgumentParser( description="A Discord bot to manage land claims and display them on Bluemap." )
parser.add_argument( '-v',"--verbose", action='store_true', help="Give more verbose update messages." )

parser.add_argument( '-d',"--dump", metavar="CSV", type=argparse.FileType('+'), help="A CSV file containing all the claims. Optional." )
parser.add_argument( '-r',"--reload", metavar="STRING", type=str, help="A bash command that will force Bluemap to reload the config. Optional." )
parser.add_argument( "--reload_timeout", metavar="seconds", type=int, default=1, help="How long to wait for the bash command to complete. Optional." )
parser.add_argument( 'inputdir', metavar="DIRECTORY", type=Path, help="The input directory of Bluemap map configuration files." )
parser.add_argument( 'outputdir', metavar="DIRECTORY", type=Path, help="The output directory to place modified versions of those files." )

args = parser.parse_args()

# check we have a valid config
if not args.inputdir.exists():
	print(f'"{args.inputdir.name}"' + " is not a valid input path! Check it's correct and can be read.")
	exit(1)

if not args.outputdir.exists():
	print(f'"{args.outputdir.name}"' + " is not a valid output path! Check it's correct and can be read.")
	exit(2)

# for each file in the input directory:
for file in args.inputdir.iterdir():

	# skip non-files
	if not file.is_file():
		continue

	# skip non-HOCON files
	conf = read_bluemap( file.absolute() )
	if result is None:
		continue
	
	# skip config files without key data
	result = parse_bluemap( conf )
	if result is None:
		continue


	# if we got here, we should be safe to concatenate
	world, db = result

        database = pd.concat( database, db )

	base_configs.append( (file.name, conf) )

	if world not in world_map:
		world_map[world] = list()
	world_map[world].append( file.name )

# is there a "dump" file? Then it overwrites what we have, on success
if args.dump:
	try:
        database = pd.read_csv( args.dump, converters={'datetime':pd.to_datetime} )
		print("Replaced the existing configuration with whatever was in the dump file.")
	except:
		print("The dump file could not be read, ignoring it.")

# TODO: tidy up column dtypes? Categorical should speed up world, for instance.

print( f"Successfully loaded, with {len(database)} claims and {len(world_map)} worlds." )

intents = discord.Intents.default()
intents.message_content = True
client = discord.Client(intents=intents)

client.run(TOKEN)

