
##### IMPORTS

from datetime import datetime
import pandas as pd
from pathlib import Path
from pyhocon import ConfigFactory
from pyhocon.config_tree import ConfigTree
from typing import Optional


##### FUNCTIONS

def read_bluemap( filename: str ) -> Optional[ConfigFactory]:
	"""
	Read in a HOCON-formatted Bluemap config file.

	filename: The path of a HOCON file to read.

	Returns a HOCON Config object on success, None on failure.
	"""
	try:
		return ConfigFactory.parse_file( filename )
	except FileNotFoundError:
        	return None     # TODO: str?

def parse_bluemap( conf: ConfigTree ) -> Optional[ tuple[str,pd.DataFrame] ]:
    """
    A helper to read the land claim data stored within a configuration file.

    conf: A HOCON ConfigTree to be read.

    Returns None if there was a major error, or a world name plus a pandas DataFrame with claims. 
	Each claim consists of a creation datetime object, a username, a jump URL, 
        a world [strs], the minimum/maximum x coordinate, the minimum/maximum 
        z coordinate [4 ints], a usage policy, and a description [strs]. Zero claims may be
	returned. 
    """

    if "land claim world" not in conf:
	return None
    world = conf["land claim world"]
   

    output = { "datetime":[], "player":[], "jump_url":[], "world":[],
		"min x":[], "max x":[], "min z":[], "max z":[],
		"policy":[], "description":[] }

    if "marker-sets" not in conf:
	return (world, pd.DataFrame(output))
    temp = conf["marker-sets"]

    if 'claims' not in temp:
	return (world, pd.DataFrame(output))
    temp = conf['claims']

    if 'markers' not in temp:
	return (world, pd.DataFrame(output))
    temp = conf['markers']

    for marker in temp:
	try:
		temp = {"world":	world,	# front-load reading from config
			"datetime":	marker["land claim date"],
			"policy":	marker["land claim policy"],
			"player":	marker["land claim player"],
			"description":	marker["land claim description"],
			"jump_url":	marker["land claim jump url"],
			"min x":	marker["shape"][0]['x'],
			"max x":	marker["shape"][2]['x'],
			"min z":	marker["shape"][0]['z'],
			"max z":	marker["shape"][1]['z'] }
		for key in temp.keys():		# only append once all reading done
			output[key].append( temp[key] )
	except:
		pass

    return (world, pd.DataFrame( output ))

def print_bluemap( file: object, out_conf: ConfigTree, subset: pd.DataFrame, stack: list(str) = list() ):
	"""
	A helper to write out a computer-friendly HOCON file.

	file: a file object to write to.
	out_conf: A HOCON ConfigTree object to write out.
	subset: A pandas DataFrame that contains the land claims FOR THIS FILE.
	stack: A list recording our current depth within the tree.
	"""
	for key in out_conf.keys():
		if (key is 'marker-sets') and (len(stack) == 0):
		
			# ensure there's a 'claims' marker set within here
			assert type(out_conf[key]) is ConfigTree
			if 'claims' not in out_conf[key].keys():
				temp = new ConfigTree()
				temp['label'] = "Claims"
        			temp['toggleable'] = True
        			temp['default-hidden'] = False
				temp['markers'] = new ConfigTree()

				out_conf[key]['claims'] = temp

			# fall through and let the rest of the code execute

        # handle the claims themselves
		if (len(stack) == 3) and (stack[-1] == 'markers') and (stack[-2] == 'claims') and \
                (stack[-3] == 'marker-sets'):
			
            for idx,row in subset.iterrows():

                file.write( (' ' *2*len(stack)) + f'marker-{idx:06d}: {{\n' )

                # hidden details first
			    file.write( (' ' *2*(len(stack)+1)) + f'"land claim date": {row["datetime"]}\n' )
			    file.write( (' ' *2*(len(stack)+1)) + f'"land claim policy": {row["policy"]}\n' )
			    file.write( (' ' *2*(len(stack)+1)) + f'"land claim player": {row["player"]}\n' )
			    file.write( (' ' *2*(len(stack)+1)) + f'"land claim description": {row["description"]}\n' )
			    file.write( (' ' *2*(len(stack)+1)) + f'"land claim jump url": {row["jump_url"]}\n' )
                file.write( '\n' )

                # now the visible portion (bounds, name, colour)
                file.write( (' ' *2*(len(stack)+1)) + f'label: {row["description"]} ({row["policy"]})\n' )
                file.write( (' ' *2*(len(stack)+1)) + 'type: "extrude"\n' )
                file.write( (' ' *2*(len(stack)+1)) + 'depth-test: true\n' )
                file.write( (' ' *2*(len(stack)+1)) + 'position: { x: ' + str( (row["min x"] + row["max x"])>>1 ) + \
                        ', y: 64, z: ' + str( (row["min z"] + row["max z")>>1 ) + '}\n' )
                file.write( (' ' *2*(len(stack)+1)) + 'shape-min-y: -64\n' )
                file.write( (' ' *2*(len(stack)+1)) + 'shape-max-y: 384\n' )
                file.write( (' ' *2*(len(stack)+1)) + 'shape: [\n' )

                file.write( (' ' *2*(len(stack)+2)) + f'{{ x: {row["min x"]}, z: {row["min z"]} }}\n' )
                file.write( (' ' *2*(len(stack)+2)) + f'{{ x: {row["min x"]}, z: {row["max z"]} }}\n' )
                file.write( (' ' *2*(len(stack)+2)) + f'{{ x: {row["max x"]}, z: {row["max z"]} }}\n' )
                # last one is automatically connected

                file.write( (' ' *2*(len(stack)+2)) + ']\n' )
                file.write( (' ' *2*(len(stack)+1)) + '}\n' )


		elif type(out_conf[key]) is ConfigTree:
			file.write( (' ' *2*len(stack)) + f'{key}: {{\n' )
			print_bluemap( file, out_conf[key], subset, stack + [key] )
			file.write( (' ' *2*len(stack)) + '}\n' )

		elif type(out_conf[key]) is str:
			# quotes are optional! This also saves us from detecting the proper quote delim
			file.write( (' ' *2*len(stack)) + f'{key}: {out_conf[key]}\n' )	
		else:
			file.write( (' ' *2*len(stack)) + f'{key}: {str(out_conf[key])}\n' )


def update_bluemap( base_configs: list[tuple[str,ConfigFactory]], out_dir: Path, database: pd.DataFrame, \
		default_world: str = 'overworld' ):
	"""
	Write out the Bluemap configuration.

	base_configs: a list of HOCON config files.
	out_dir: A Path object representing the target directory.
	database: A pandas DataFrame containing the claims.
	"""

	# for each base configuration file:
	for out_file, out_conf in base_configs:

		#  check if the upper level contains land-claim-world; if not, substitute the default
		if 'land claim world' not in out_conf.keys() or \
				type(out_conf['land claim world']) is not str:
			out_conf['land claim world'] = default_world

		subset = database[ database['world'] == out_conf['land claim world'] ]

		#  open the matching output file
		with open( f'{out_dir}/{out_file}', 'wt' ) as file:

			#  fire off the contents of the config file
			print_bluemap( file, out_conf, subset, 0 )

