#!/usr/bin/env python3

##### IMPORTS

import claims
import pandas as pd
from unittest import main, TestCase

##### GLOBALS


default_map = { 
            'overworld':['one.conf'],
            'nether':   ['two.conf'],
            'end':      ['three.conf']
            }

##### FUNCTIONS


##### CLASSES

class TestClaimCode(TestCase):

    def test_ideal_claim(self):

        result = claims.parse_claim( \
                "(-16,-17) - (18,19) [overworld, public] Happy Hobbit Hole\n", default_map )
        self.assertNotEqual( type(result), str, msg=f"Errored out with {result}." )
        self.assertEqual( type(result), tuple, msg=f"Expected a tuple to be returned." )
        self.assertEqual( len(result), 7, msg=f"Expected a septuple to be returned." )

        world, min_x, max_x, min_z, max_z, policy, desc = result

        self.assertEqual( world, 'overworld', msg=f"Incorrect world detected." )
        self.assertEqual( policy, 'public', msg=f"Incorrect policy detected." )
        self.assertEqual( desc, 'Happy Hobbit Hole', msg=f"Incorrect description extracted." )

        self.assertEqual( min_x, -16, msg=f"Incorrect min_x value." )
        self.assertEqual( max_x, 18, msg=f"Incorrect max_x value." )
        self.assertEqual( min_z, -17, msg=f"Incorrect min_z value." )
        self.assertEqual( max_z, 19, msg=f"Incorrect max_z value." )

    def test_claim_variants(self):

        result = claims.parse_claim( \
                "(-11,-12) -> (13,14) [overworld, public] Happy Hobbit Hole\n", default_map )
        self.assertNotEqual( type(result), str, msg=f"Errored out with {result}." )
        self.assertEqual( type(result), tuple, msg=f"Expected a tuple to be returned." )
        self.assertEqual( len(result), 7, msg=f"Expected a septuple to be returned." )

        world, min_x, max_x, min_z, max_z, policy, desc = result

        self.assertEqual( world, 'overworld', msg=f"Incorrect world detected." )
        self.assertEqual( policy, 'public', msg=f"Incorrect policy detected." )
        self.assertEqual( desc, 'Happy Hobbit Hole', msg=f"Incorrect description extracted." )

        self.assertEqual( min_x, -11, msg=f"Incorrect min_x value." )
        self.assertEqual( max_x, 13, msg=f"Incorrect max_x value." )
        self.assertEqual( min_z, -12, msg=f"Incorrect min_z value." )
        self.assertEqual( max_z, 14, msg=f"Incorrect max_z value." )

        result = claims.parse_claim( \
                "(-11.1,-12.2) - (13.3,14.4) [public, overworld] Happy Hobbit Hole\n", default_map )
        self.assertNotEqual( type(result), str, msg=f"Errored out with {result}." )
        self.assertEqual( type(result), tuple, msg=f"Expected a tuple to be returned." )
        self.assertEqual( len(result), 7, msg=f"Expected a septuple to be returned." )

        world, min_x, max_x, min_z, max_z, policy, desc = result

        self.assertEqual( world, 'overworld', msg=f"Incorrect world detected." )
        self.assertEqual( policy, 'public', msg=f"Incorrect policy detected." )
        self.assertEqual( desc, 'Happy Hobbit Hole', msg=f"Incorrect description extracted." )

        self.assertEqual( min_x, -11, msg=f"Incorrect min_x value." )
        self.assertEqual( max_x, 13, msg=f"Incorrect max_x value." )
        self.assertEqual( min_z, -12, msg=f"Incorrect min_z value." )
        self.assertEqual( max_z, 14, msg=f"Incorrect max_z value." )

        result = claims.parse_claim( \
                "(-10/-11) -> (12,13) Happy Hobbit Hole\n", default_map )
        self.assertNotEqual( type(result), str, msg=f"Errored out with {result}." )
        self.assertEqual( type(result), tuple, msg=f"Expected a tuple to be returned." )
        self.assertEqual( len(result), 7, msg=f"Expected a septuple to be returned." )

        world, min_x, max_x, min_z, max_z, policy, desc = result

        self.assertEqual( world, list(default_map.keys())[0], msg=f"Incorrect world detected." )
        self.assertEqual( policy, list(claims.policies.keys())[0], msg=f"Incorrect policy detected." )
        self.assertEqual( desc, 'Happy Hobbit Hole', msg=f"Incorrect description extracted." )

        self.assertEqual( min_x, -10, msg=f"Incorrect min_x value." )
        self.assertEqual( max_x, 12, msg=f"Incorrect max_x value." )
        self.assertEqual( min_z, -11, msg=f"Incorrect min_z value." )
        self.assertEqual( max_z, 13, msg=f"Incorrect max_z value." )

        result = claims.parse_claim( \
                "(-10,-11) - (12/13) [public] Happy Hobbit Hole\n", default_map )
        self.assertNotEqual( type(result), str, msg=f"Errored out with {result}." )
        self.assertEqual( type(result), tuple, msg=f"Expected a tuple to be returned." )
        self.assertEqual( len(result), 7, msg=f"Expected a septuple to be returned." )

        world, min_x, max_x, min_z, max_z, policy, desc = result

        self.assertEqual( world, list(default_map.keys())[0], msg=f"Incorrect world detected." )
        self.assertEqual( policy, 'public', msg=f"Incorrect policy detected." )
        self.assertEqual( desc, 'Happy Hobbit Hole', msg=f"Incorrect description extracted." )

        self.assertEqual( min_x, -10, msg=f"Incorrect min_x value." )
        self.assertEqual( max_x, 12, msg=f"Incorrect max_x value." )
        self.assertEqual( min_z, -11, msg=f"Incorrect min_z value." )
        self.assertEqual( max_z, 13, msg=f"Incorrect max_z value." )

        result = claims.parse_claim( \
                "(-10/-11) -> (12/13) [end] Happy Hobbit Hole\n", default_map )
        self.assertNotEqual( type(result), str, msg=f"Errored out with {result}." )
        self.assertEqual( type(result), tuple, msg=f"Expected a tuple to be returned." )
        self.assertEqual( len(result), 7, msg=f"Expected a septuple to be returned." )

        world, min_x, max_x, min_z, max_z, policy, desc = result

        self.assertEqual( world, "end", msg=f"Incorrect world detected." )
        self.assertEqual( policy, list(claims.policies.keys())[0], msg=f"Incorrect policy detected." )
        self.assertEqual( desc, 'Happy Hobbit Hole', msg=f"Incorrect description extracted." )

        self.assertEqual( min_x, -10, msg=f"Incorrect min_x value." )
        self.assertEqual( max_x, 12, msg=f"Incorrect max_x value." )
        self.assertEqual( min_z, -11, msg=f"Incorrect min_z value." )
        self.assertEqual( max_z, 13, msg=f"Incorrect max_z value." )

        result = claims.parse_claim( \
                "(-10,-11)-(12,13)[private]Happy Hobbit Hole", default_map )
        self.assertNotEqual( type(result), str, msg=f"Errored out with {result}." )
        self.assertEqual( type(result), tuple, msg=f"Expected a tuple to be returned." )
        self.assertEqual( len(result), 7, msg=f"Expected a septuple to be returned." )

        world, min_x, max_x, min_z, max_z, policy, desc = result

        self.assertEqual( world, list(default_map.keys())[0], msg=f"Incorrect world detected." )
        self.assertEqual( policy, "private", msg=f"Incorrect policy detected." )
        self.assertEqual( desc, 'Happy Hobbit Hole', msg=f"Incorrect description extracted." )

        self.assertEqual( min_x, -10, msg=f"Incorrect min_x value." )
        self.assertEqual( max_x, 12, msg=f"Incorrect max_x value." )
        self.assertEqual( min_z, -11, msg=f"Incorrect min_z value." )
        self.assertEqual( max_z, 13, msg=f"Incorrect max_z value." )

        result = claims.parse_claim( \
                "(10/11)-(12/13)Happy Hobbit Hole", default_map )
        self.assertNotEqual( type(result), str, msg=f"Errored out with {result}." )
        self.assertEqual( type(result), tuple, msg=f"Expected a tuple to be returned." )
        self.assertEqual( len(result), 7, msg=f"Expected a septuple to be returned." )

        world, min_x, max_x, min_z, max_z, policy, desc = result

        self.assertEqual( world, list(default_map.keys())[0], msg=f"Incorrect world detected." )
        self.assertEqual( policy, list(claims.policies.keys())[0], msg=f"Incorrect policy detected." )
        self.assertEqual( desc, 'Happy Hobbit Hole', msg=f"Incorrect description extracted." )

        self.assertEqual( min_x, 10, msg=f"Incorrect min_x value." )
        self.assertEqual( max_x, 12, msg=f"Incorrect max_x value." )
        self.assertEqual( min_z, 11, msg=f"Incorrect min_z value." )
        self.assertEqual( max_z, 13, msg=f"Incorrect max_z value." )

        result = claims.parse_claim( \
                "(20/11)-(12/31)[end]Amazing\n", default_map )
        self.assertNotEqual( type(result), str, msg=f"Errored out with {result}." )
        self.assertEqual( type(result), tuple, msg=f"Expected a tuple to be returned." )
        self.assertEqual( len(result), 7, msg=f"Expected a septuple to be returned." )

        world, min_x, max_x, min_z, max_z, policy, desc = result

        self.assertEqual( world, "end", msg=f"Incorrect world detected." )
        self.assertEqual( policy, list(claims.policies.keys())[0], msg=f"Incorrect policy detected." )
        self.assertEqual( desc, 'Amazing', msg=f"Incorrect description extracted." )

        self.assertEqual( min_x, 12, msg=f"Incorrect min_x value." )
        self.assertEqual( max_x, 20, msg=f"Incorrect max_x value." )
        self.assertEqual( min_z, 11, msg=f"Incorrect min_z value." )
        self.assertEqual( max_z, 31, msg=f"Incorrect max_z value." )

        result = claims.parse_claim( \
                "(10/41)-(12/-51) [private]Happy Hobbit Hole", default_map )
        self.assertNotEqual( type(result), str, msg=f"Errored out with {result}." )
        self.assertEqual( type(result), tuple, msg=f"Expected a tuple to be returned." )
        self.assertEqual( len(result), 7, msg=f"Expected a septuple to be returned." )

        world, min_x, max_x, min_z, max_z, policy, desc = result

        self.assertEqual( world, list(default_map.keys())[0], msg=f"Incorrect world detected." )
        self.assertEqual( policy, "private", msg=f"Incorrect policy detected." )
        self.assertEqual( desc, 'Happy Hobbit Hole', msg=f"Incorrect description extracted." )

        self.assertEqual( min_x, 10, msg=f"Incorrect min_x value." )
        self.assertEqual( max_x, 12, msg=f"Incorrect max_x value." )
        self.assertEqual( min_z, -51, msg=f"Incorrect min_z value." )
        self.assertEqual( max_z, 41, msg=f"Incorrect max_z value." )

        result = claims.parse_claim( \
                "(10/41)-(12/-51) [private,end] Happy Hobbit Hole", default_map )
        self.assertNotEqual( type(result), str, msg=f"Errored out with {result}." )
        self.assertEqual( type(result), tuple, msg=f"Expected a tuple to be returned." )
        self.assertEqual( len(result), 7, msg=f"Expected a septuple to be returned." )

        world, min_x, max_x, min_z, max_z, policy, desc = result

        self.assertEqual( world, "end", msg=f"Incorrect world detected." )
        self.assertEqual( policy, "private", msg=f"Incorrect policy detected." )
        self.assertEqual( desc, 'Happy Hobbit Hole', msg=f"Incorrect description extracted." )

        self.assertEqual( min_x, 10, msg=f"Incorrect min_x value." )
        self.assertEqual( max_x, 12, msg=f"Incorrect max_x value." )
        self.assertEqual( min_z, -51, msg=f"Incorrect min_z value." )
        self.assertEqual( max_z, 41, msg=f"Incorrect max_z value." )

    def test_claim_failures(self):

        result = claims.parse_claim( \
                "10,11 -> 12,13 [private] Happy Hobbit Hole", default_map )
        self.assertEqual( type(result), str, msg=f"Did not error out: {result}." )

        result = claims.parse_claim( \
                "(10,11) - 12,13 [end,private] Happy Hobbit Hole", default_map )
        self.assertEqual( type(result), str, msg=f"Did not error out: {result}." )

        result = claims.parse_claim( \
                "10,11 -> (12,13) [nether] Huh?", default_map )
        self.assertEqual( type(result), str, msg=f"Did not error out: {result}." )

        result = claims.parse_claim( \
                "(10,11) - (a,13) [end,private] Happy Hobbit Hole", default_map )
        self.assertEqual( type(result), str, msg=f"Did not error out: {result}." )

        result = claims.parse_claim( \
                "(10,B)->(12,13) [nether] Huh?", default_map )
        self.assertEqual( type(result), str, msg=f"Did not error out: {result}." )

        result = claims.parse_claim( \
                "(10,11) - (12,13) [end,private,end] Happy Hobbit Hole", default_map )
        self.assertEqual( type(result), str, msg=f"Did not error out: {result}." )

    def test_collisions(self):

        database = pd.read_csv( 'test_files/test_dump.csv', converters={'datetime':pd.to_datetime} )

        result = claims.find_collisions( database, ('nether',-16,17,-18,19) )
        self.assertEqual( result, None, msg=f"Found a match when we expected none." )

        result = claims.find_collisions( database, ('overworld',-16,17,-18,19) )
        self.assertEqual( type(result), pd.DataFrame, msg=f"Failed to find an exact match when we expected one." )
        self.assertEqual( len(result), 1, msg=f"Expected just one match." )

        result = claims.find_collisions( database, ('overworld',-32,-15,-18,19) )
        self.assertEqual( type(result), pd.DataFrame, msg=f"Failed to find an overlapping match when we expected one." )
        self.assertEqual( len(result), 1, msg=f"Expected just one match." )

        result = claims.find_collisions( database, ('overworld',-32,-16,-18,19) )
        self.assertEqual( type(result), pd.DataFrame, msg=f"Overlapping maxima/minima should NOT be permitted." )
        self.assertEqual( len(result), 1, msg=f"Expected just one match." )

        result = claims.find_collisions( database, ('overworld',-32,-17,-18,19) )
        self.assertEqual( result, None, msg=f"The queried claim is well outside any existing one." )

        result = claims.find_collisions( database, ('overworld',16,32,-18,19) )
        self.assertEqual( type(result), pd.DataFrame, msg=f"Failed to find an overlapping match when we expected one." )
        self.assertEqual( len(result), 1, msg=f"Expected just one match." )

        result = claims.find_collisions( database, ('overworld',17,32,-18,19) )
        self.assertEqual( type(result), pd.DataFrame, msg=f"Overlapping maxima/minima should NOT be permitted." )
        self.assertEqual( len(result), 1, msg=f"Expected just one match." )

        result = claims.find_collisions( database, ('overworld',18,32,-18,19) )
        self.assertEqual( result, None, msg=f"The queried claim is well outside any existing one." )

if __name__ == '__main__':
        main()
